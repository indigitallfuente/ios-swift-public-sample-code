//
//  NotificationService.h
//  Notification
//
//  Created by indigitall on 10/4/23.
//  Copyright © 2023 indigitall. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>
#import <Indigitall/Indigitall.h>
@interface NotificationService : UNNotificationServiceExtension

@end
