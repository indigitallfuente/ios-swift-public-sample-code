//
//  NotificationService.m
//  Notification
//
//  Created by indigitall on 10/4/23.
//  Copyright © 2023 indigitall. All rights reserved.
//

#import "NotificationService.h"

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;
@property (nonatomic, strong) UNNotificationRequest *request;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    self.request = request;

    [Indigitall didReceiveNotificationRequest:self.request withContentHandler:self.contentHandler];

}

- (void)serviceExtensionTimeWillExpire {
    if (self.contentHandler != nil && self.bestAttemptContent != nil){
        [Indigitall serviceExtensionTimeWillExpire:self.bestAttemptContent withContentHandler:self.contentHandler];
    }
}

@end
