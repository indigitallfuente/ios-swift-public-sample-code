//
//  ViewController.swift
//  IOS-public-simple-code-swift
//
//  Created by indigitall on 17/1/19.
//  Copyright © 2019 indigitall. All rights reserved.
//

import UIKit
import Indigitall

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        //
        let config = INPushConfig(appKey: "537a3609-299f-4fc1-8c5e-a74a4fa3a2d2")
        config.pushPermissionMode = provisional
        config.locationPermissionMode = automatic
        config.setProductName(INDefaults.getProductName())
        config.setProductVersion(INDefaults.getProductVersion())
        config.debugMode = IN_DEBUG
        
        
        let alertViewController = UIAlertController(title: "LOADING...", message: nil, preferredStyle: .alert)
        present(alertViewController, animated: true, completion: nil)
        
        Indigitall.initialize(with: config, onIndigitallInitialized: { (permissions, device) in
            DispatchQueue.main.async {
                alertViewController.dismiss(animated: true, completion: {
                    for i in 0..<permissions.count {
                        var typeString = ""
                        switch (permissions[i].permissionType) {
                            case push:
                                typeString = "push"
                                break
                            case location:
                                typeString = "location"
                                break;
                            default:
                                break;
                        }
                        
                        var statusString = "notDeterminated"
                        switch (permissions[i].permissionStatus ) {
                            case accepted:
                                statusString = "accepted"
                                break
                            case denied:
                                statusString = "denied"
                                break;
                            default:
                                break;
                        }
                        
                        print("onIndigitallInitialized \(typeString) permission: \(statusString)")
                                               
                         
                    }
                    print("onIndigitallInitialized DEVICE: \(device.deviceID )")
                    
                })
            }
        }) { (error) in
            print("ERROR: \(error.message)")
            
        }
        
        generatepair()
       
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Indigitall.askNotificationPermission()
    }
    
    func generatepair(){
        var statusCode: OSStatus
        var publicKey: SecKey?
        var privateKey: SecKey?

        let publicKeyAttribute: [NSObject : NSObject] = [kSecAttrIsPermanent: true as NSObject, kSecAttrApplicationTag: "com.indigitall.keys.copy3MyPublicKey".data(using: String.Encoding.utf8)! as NSObject]

        let privateKeyAtrribute: [NSObject: NSObject] = [kSecAttrIsPermanent: true as NSObject, kSecAttrApplicationTag: "com.indigitall.keys.copy3MyPrivateKey".data(using: String.Encoding.utf8)! as NSObject]

        var keyPairAttr = [NSObject: Any]()
        keyPairAttr[kSecAttrType] = kSecAttrKeyTypeRSA
        keyPairAttr[kSecAttrKeySizeInBits] = 2048
        keyPairAttr[kSecReturnData] = true
        keyPairAttr[kSecPublicKeyAttrs] = publicKeyAttribute
        keyPairAttr[kSecPrivateKeyAttrs] = privateKeyAtrribute


        statusCode = SecKeyGeneratePair(keyPairAttr as CFDictionary, &publicKey, &privateKey)
        
        print("statuscode \(statusCode)")
        
        if #available(iOS 10.0, *) {
            let publicKeyData = SecKeyCopyExternalRepresentation(publicKey!, nil)
            print("publicKey \((publicKeyData as NSData? as Data?)!.base64EncodedString())")
            
            let keyType = kSecAttrKeyTypeRSA
            let keySize = 2048
            let publicKeyDER = CryptoExportImportManager.exportRSAPublicKeyToDER((publicKeyData as NSData? as Data?)!, keyType: keyType as String, keySize: keySize)
            let publicKeyPEM = CryptoExportImportManager.PEMKeyFromDERKey(publicKeyDER)
            print("PEM: \(publicKeyPEM)")
        } else {
            // Fallback on earlier versions
        }
        

        
    }

}

