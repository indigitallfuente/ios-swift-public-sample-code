//
//  INCoreExternalApp.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "INAPIConstants.h"

NS_ASSUME_NONNULL_BEGIN

@interface INCoreExternalApp : NSObject
// MARK: - Properties
/// The external app indigitall ID
@property (nonatomic) int iNExternalAppId;
/// A string containing the name of the app
@property (nonatomic) NSString *name;
/// A string containing the external app url scheme
@property (nonatomic) NSString *iosCode;

- (id) init: (NSDictionary *)json;
- (int) getINExternalAppId;
- (NSMutableDictionary *)toJson;
@end

NS_ASSUME_NONNULL_END
