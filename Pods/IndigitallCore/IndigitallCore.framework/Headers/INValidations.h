//
//  Validations.h
//  Indigitall
//
//  Created by indigitall on 22/3/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INValidations : NSObject

+ (BOOL) isAppKeyFormat;
+ (BOOL) isExternalIdFormat;
+ (BOOL) isDeviceIdFormat: (NSString * _Nullable) deviceId;
+ (BOOL) isValidFormatRequest;
+ (BOOL) isValidFormatWithExternalIdRequest;

@end

NS_ASSUME_NONNULL_END
