//
//  INCoreDevice.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INCoreExternalApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INCoreDevice : NSObject

@property (nonatomic, readonly) NSString * JSON_EXTERNAL_CODE;
@property (nonatomic, readonly) NSString * JSON_APP_VERSION;
@property (nonatomic, readonly) NSString * JSON_CARRIER;
@property (nonatomic, readonly) NSString * JSON_DEVICE_BRAND;
@property (nonatomic, readonly) NSString * JSON_DEVICE_MODEL;
@property (nonatomic, readonly) NSString * JSON_DEVICE_TYPE;
@property (nonatomic, readonly) NSString * JSON_ENABLED;
@property (nonatomic, readonly) NSString * JSON_LOCALE;
@property (nonatomic, readonly) NSString * JSON_EXTERNAL_APPLICATIONS;
@property (nonatomic, readonly) NSString * JSON_OS_NAME;
@property (nonatomic, readonly) NSString * JSON_OS_VERSION;
@property (nonatomic, readonly) NSString * JSON_SDK_VERSION;
@property (nonatomic, readonly) NSString * JSON_PLATFORM;
@property (nonatomic, readonly) NSString * JSON_PRODUCT_NAME;
@property (nonatomic, readonly) NSString * JSON_PRODUCT_VERSION;
@property (nonatomic, readonly) NSString * JSON_TIME_OFFSET;
@property (nonatomic, readonly) NSString * JSON_TIME_ZONE;
/// Array of external apps
@property (nonatomic, nullable) NSArray<INCoreExternalApp *> *externalApplications;
/// Current platform
@property (nonatomic) NSString *platform;
/// Third party product name
@property (nonatomic) NSString *productName;
/// Third party product version
@property (nonatomic) NSString *productVersion;
/// Operative system name
@property (nonatomic) NSString *osName;
/// Operative system versión
@property (nonatomic) NSString *osVersion;
/// Device brand
@property (nonatomic) NSString *deviceBrand;
/// Device model
@property (nonatomic) NSString *deviceModel;
- (NSString *)getModelIdentifier;
/// Carrier
@property (nonatomic) NSString *carrier;
- (NSString *)getCarrier;
/// Device type (mobile or tablet)
@property (nonatomic) NSString *deviceType;
+ (NSString *) getDeviceType ;
/// Current app version
@property (nonatomic) NSString *appVersion;
/// Device preferred languages
@property (nonatomic) NSString *locale;
/// Current time zone
@property (nonatomic) NSString *timeZone;
/// Current time offset in hours
@property (nonatomic) long timeOffset;
/// Current external code
@property (nonatomic, nullable) NSString *externalCode;

@property (nonatomic) BOOL configEnabled;

@property (nonatomic) NSString *sdkVersion;

@property (nonatomic) NSNumber *enabled;
- (id)init;
- (id)initWithNoData;
- (id)initWithData: (NSMutableDictionary * _Nullable) data;

- (BOOL) getConfigEnabled;

- (void) update: (NSDictionary *)json;
- (id) saveExternalCode: (NSString *_Nullable)code;
@end

NS_ASSUME_NONNULL_END
