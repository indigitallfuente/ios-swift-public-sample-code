//
//  CoreLocationHandler.h
//  Indigitall
//
//  Created by indigitall on 21/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCoreLocationHandler : CLLocationManager<CLLocationManagerDelegate>

@property (nonatomic)INCoreLocationHandler * _Nullable shared;

typedef enum{
    notWasDeterminated,
    allowed,
    notAllowed
}LocationStatus;

//@property (nonatomic) CLLocation * _Nullable lastLocation;
@property (nonatomic) int locationStatus;
@property (nonatomic) bool locationAllowed;
@property (nonatomic) CLLocationManager * _Nullable locationManager;

- (id) init;
- (void) startMonitoringWithAsk: (BOOL) ask;
- (void) configLocationHasChanged;
- (void) askPermission;
- (void) askPermissionBySystem;

- (void) startMonitoring;
- (void) sendAskEvent;
- (void) sendEvent:(int )status;
- (void) setLastLocation:(CLLocation *)lastLocation;

- (void) locationManager:(CLLocationManager *_Nullable)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
- (void) locationManager:(CLLocationManager *_Nullable)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations;

@end

NS_ASSUME_NONNULL_END
