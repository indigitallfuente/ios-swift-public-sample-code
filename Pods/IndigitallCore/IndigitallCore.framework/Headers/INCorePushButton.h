//
//  INCorePushButton.h
//  IndigitallCore
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCorePushButton : NSObject
@property NSString *label;
@property NSArray *topics;

-(id)init:(NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
