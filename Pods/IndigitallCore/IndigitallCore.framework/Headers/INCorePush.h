//
//  INCorePush.h
//  IndigitallCore
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCorePush : NSObject

// MARK: - Properties
/// The push indigitall ID
@property (nonatomic) long pushId;
/// Public app identifier from indigitall
@property (nonatomic) NSString *appKey;
/// A string containing the title of the push
@property (nonatomic) NSString *title;
/// A string containing the body of the push
@property (nonatomic) NSString *body;
/// A string containing the url of the push image
@property (nonatomic) NSString *image;
/// A string containing the url of the push gif
@property (nonatomic) NSString *gif;
/// Astring containing the extra info appended in the indigitall panel
@property (nonatomic) NSString *data;
/// Secured data encrypted
@property (nonatomic) NSString *securedData;
/// SendingId
@property (nonatomic) NSString *sendingId;
/// Campign Id
@property (nonatomic) NSString *campaignId;
/// journeyStateId
@property (nonatomic) int journeyStateId;
/// journeyExecution
@property (nonatomic) int journeyExecution;
/// cjCurrentStateId
@property (nonatomic) int cjCurrentStateId;


- (id)init: (NSDictionary *)json;


@end

NS_ASSUME_NONNULL_END
