//
//  INDefaults.h
//  indigitall+objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "INCoreExternalApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INDefaults : NSObject

@property (nonatomic)NSString *appKey;
@property (nonatomic)NSString *deviceKey;
@property (nonatomic)NSString *productNameKey;
@property (nonatomic)NSString *productVersionKey;
@property (nonatomic)NSString *locationPermissionAskedKey;
@property (nonatomic)NSString *locationPermissionModeKey;
@property (nonatomic)NSString *locationPermissionStatusSentKey;
@property (nonatomic)NSString *visitEventTimestampKey;
@property (nonatomic)NSString *enabledKey;
@property (nonatomic)NSString *configEnabledKey;
@property (nonatomic)NSString *configServiceSyncTimeKey;
@property (nonatomic)NSString *configLastCallTimestampKey;
@property (nonatomic)NSString *configMaintenanceTimeStartKey;
@property (nonatomic)NSString *configMaintenanceTimeEndKey;
@property (nonatomic)NSString *configLocationEnabledKey;
@property (nonatomic)NSString *domain;
@property (nonatomic)NSString *domainInbox;
@property (nonatomic)NSString *externalId;
@property (nonatomic)NSString *inAppEnabled;
@property (nonatomic)NSString *secureSendingEnabledKey;
@property (nonatomic)NSString *secureSendingAppPublicKeyKey;
@property (nonatomic)NSString *sdkVersionKey;
@property (nonatomic)NSString *externalAppsKey;
@property (nonatomic)NSString *externalAppsForPostMethodKey;
@property (nonatomic)NSString *latitudeKey;
@property (nonatomic)NSString *longitudeKey;
@property (nonatomic)NSString *debugLogKey;
@property (nonatomic)NSString *authModeKey;

//- (id) init;
+ (void) setDeviceId:(NSString *) deviceId;
+ (NSString *) getDeviceId;

+ (void) setAppKey:(NSString *) appKey;
+ (NSString *) getAppKey;

+ (void) setProductName:(NSString *) productName;
+ (NSString *) getProductName;
+ (void) setProductVersion:(NSString *) productVersion;
+ (NSString *) getProductVersion;

+ (BOOL) getLocationPermissionAsked;
+ (void) setLocationPermissionAsked:(BOOL) locationPermissionAsked;
+ (int) getLocationPermissionStatus;
+ (void) setLocationPermissionStatus:(int ) locationPermissionStatus;
+ (int) getLocationPermissionMode;
+ (void) setLocationPermissionMode:(int ) locationPermissionMode;

+ (int64_t) getEventVisitTimestamp;
+ (void) setEventVisitTimestamp:(int64_t) eventVisitTimestamp;

+ (void) setEnabled:(BOOL) enabled;
+ (BOOL) getEnabled;

+ (void) setConfigEnabled:(BOOL) enabled;
+ (BOOL) getConfigEnabled;

+ (void) setConfigServiceSyncTime:(int) config;
+ (int) getConfigServiceSyncTime;

+ (void) setConfigLastCallTimestamp:(int64_t) config;
+ (int64_t) getConfigLastCallTimestamp;

+ (void) setConfigMaintenanceTimeStart:(NSString *) config;
+ (NSString *) getConfigMaintenanceTimeStart;

+ (void) setConfigMaintenanceTimeEnd:(NSString *) config;
+ (NSString *) getConfigMaintenanceTimeEnd;

+ (void) setConfigLocationEnabled:(BOOL) config;
+ (BOOL) getConfigLocationEnabled;

+ (void) setDomain:(NSString *)domain;
+ (NSString *) getDomain;

+ (void) setDomainInbox:(NSString *)domainInbox ;
+ (NSString *) getDomainInbox;

+ (void) setExternalId:(NSString * _Nullable)externalId;
+ (NSString *) getExternalId;

+ (void) setInAppEnabled :(BOOL)inAppEnabled ;
+ (BOOL) getInAppEnabled;

+ (void) setSecureSendingEnabled:(BOOL)secureSendingEnabled;
+ (BOOL) getSecureSendingEnabled;

+ (void) setSecureSendingAppPublicKey: (NSString *)secureSendingAppPublicKey;
+ (NSString *) getSecureSendingAppPublicKey;

+ (void) setSDKVersion:(NSString *)sdkVersion;
+ (NSString *) getSDKVersion;


+ (void) setExternalApps:(NSArray<INCoreExternalApp *> * _Nullable )externalApps ;
+ (NSArray<INCoreExternalApp *> *) getExternalApps;

+ (void) setExternalAppsForPostMethod:(NSArray<INCoreExternalApp *> * _Nullable )externalApps ;
+ (NSArray<INCoreExternalApp *> *) getExternalAppsForPostMethod;


+ (void) setLatitude:(double)latitude;
+ (double) getLatitude;

+ (void) setLongitude:(double)longitude;
+ (double) getLongitude;

+ (void) setLogDebug:(int)debugLog;
+ (int) getLogDebug;

+ (void) setAuthMode:(NSString *)authMode;
+ (NSString *) getAuthMode;

@end

NS_ASSUME_NONNULL_END
