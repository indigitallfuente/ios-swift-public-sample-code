//
//  INErrorCode.h
//  Indigitall
//
//  Created by indigitall on 29/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum {
    GENERAL_ERROR = 600,
    API_SERVER_ERROR = 500,
    API_PARAMETER_MISSING = 400,
    API_APPKEY_NOT_VALID = 401,
    API_FORBIDDEN_REQUEST = 403,
    API_DEVICE_NOT_FOUND = 404,
    API_TOPICS_ARE_INSERTED = 409,
    BAD_REQUEST_SERVER_ERROR = 4800
} INErrorCode;

extern NSString* _Nonnull const FormatStateError_toString[];


NS_ASSUME_NONNULL_END
