//
//  EventCallback.h
//  Indigitall
//
//  Created by indigitall on 18/5/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface INEventCallback : INBaseCallback

typedef void(^onErrorEvent)(INError * error);
@property (readwrite, copy) onErrorEvent onError;

typedef void(^onSuccessEvent)(void);
@property (readwrite, copy) onSuccessEvent onSuccess;

- (id)initWithOnSuccess: (void(^)(void))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
