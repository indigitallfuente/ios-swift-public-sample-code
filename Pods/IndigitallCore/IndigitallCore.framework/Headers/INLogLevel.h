//
//  INLogLevel.h
//  Indigitall
//
//  Created by indigitall on 21/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum {
    IN_DEBUG = 1,
    IN_INFO = 2,
    IN_WARNING = 3,
    IN_ERROR = 4
} INLogLevel;

//extern NSString* _Nonnull const FormatStateLogLevel_toString[];
NS_ASSUME_NONNULL_END
