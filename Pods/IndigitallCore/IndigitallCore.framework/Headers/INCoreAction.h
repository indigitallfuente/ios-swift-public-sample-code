//
//  INCoreAction.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCoreAction : NSObject

// Types
/// An enum representing the action type
///
/// - openApp: the app will be opened
/// - openURL: the app will open an external url
/// - openWebview: the app will open an url inside a Webview
/// - call: the app will make a phone call
/// - market: the app will open an AppStore app site
/// - share: the app will show an actionsheet to share a message
typedef enum{
    /// The app will be opened
    app,
    /// The app will open an external url
    url,
    /// The app will open a url inside the app itself
    webview,
    /// The app will make a phone call
    call,
    /// The app will open an AppStore app site
    market,
    /// The app will show an actionsheet to share a message
    share,
    /// The app do nothing
    noAction,
    /// The app show or download *pkpass
    wallet
}ActionType;

// Properties
/// Inidcates the action type
@property (nonatomic) ActionType type;
/// A bool that inidicates if the action is destructive
@property (nonatomic) bool destroy;
/// A string containing the value to use for the different action types
@property (nonatomic) NSString *app;
@property (nonatomic) NSString *url;
@property (nonatomic) NSString *webview;
@property (nonatomic) NSString *share;
@property (nonatomic) NSString *call;
@property (nonatomic) NSString *market;
@property (nonatomic) NSString *noAction;
@property (nonatomic) NSString *wallet;
/// An array containing the topics which the user will be subscribed
@property (nonatomic) NSArray *topics;

- (id) init: (NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
