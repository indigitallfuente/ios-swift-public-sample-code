//
//  INApplication.h
//  Indigitall
//
//  Created by indigitall on 03/10/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INCoreExternalApp.h>
#import "INPushError.h"

NS_ASSUME_NONNULL_BEGIN

@interface INApplication : NSObject

@property (nonatomic) NSMutableDictionary *configuration;
@property (nonatomic) NSMutableArray<INCoreExternalApp *> *externalApps;

- (id)init: (NSDictionary *) json;
@end

NS_ASSUME_NONNULL_END
