//
//  INCurrentData.h
//  Indigitall
//
//  Created by indigitall on 17/01/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCurrentData : NSObject

@property (nonatomic) NSMutableArray<NSNumber *> *notificationIdList;

+ (id) current;

@end

NS_ASSUME_NONNULL_END
