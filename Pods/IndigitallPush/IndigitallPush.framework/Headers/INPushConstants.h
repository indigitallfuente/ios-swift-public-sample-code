//
//  INPushConstants.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INAPIConstants.h>

NS_ASSUME_NONNULL_BEGIN

@interface INPushConstants : INAPIConstants
@property (nonatomic, class, readonly) NSString * pushId;
@property (nonatomic, class, readonly) NSString * appKey;
@property (nonatomic, class, readonly) NSString * title;
@property (nonatomic, class, readonly) NSString * body;
@property (nonatomic, class, readonly) NSString * image;
@property (nonatomic, class, readonly) NSString * gif;
@property (nonatomic, class, readonly) NSString * data;

@property (nonatomic, class, readonly) NSString * securedData;
@property (nonatomic, class, readonly) NSString * securedKey;
@property (nonatomic, class, readonly) NSString * PUSH_ID;
@property (nonatomic, class, readonly) NSString * SENDING_ID;
@property (nonatomic, class, readonly) NSString * CAMPAIGN_ID;
@property (nonatomic, class, readonly) NSString * JOURNEY_STATE_ID;
@property (nonatomic, class, readonly) NSString * CJ_CURRENT_STATE_ID;
@property (nonatomic, class, readonly) NSString * JOURNEY_EXECUTION;
@property (nonatomic, class, readonly) NSString * device_deviceID;
@property (nonatomic, class, readonly) int64_t PUT_REQUEST_TIMESTAMP;
@end

NS_ASSUME_NONNULL_END
