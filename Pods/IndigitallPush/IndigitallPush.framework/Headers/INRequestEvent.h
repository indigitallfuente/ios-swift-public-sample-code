//
//  RequestEvent.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INPush.h"
#import <IndigitallCore/INBaseRequest.h>
#import "INPermissions.h"
NS_ASSUME_NONNULL_BEGIN

@interface INRequestEvent : INBaseRequest

typedef enum{
    click,
    receive,
    send,
    dismiss
}EventType;

typedef enum{
    accept,
    reject,
    ask
}EventTypePermission;

typedef enum{
    update
}EventTypeLocation;

//typedef enum{
//    push,
//    location
//}PermissionType;

@property (nonatomic)NSString *rawValue;

@property (nonatomic) INPush *push;
@property (nonatomic) EventType *eventType;
@property (nonatomic) int clickedButton;

@property (nonatomic) NSString * eventTypePermission;
@property (nonatomic) NSString * permissionType;

@property (nonatomic) EventTypeLocation *eventTypeLocation;
@property (nonatomic) double latitude;
@property (nonatomic) double longitud;
@property (nonatomic) NSString *deviceId;
@property (nonatomic) NSString *externalId;

@property (nonatomic) NSString *customEvent;
@property (nonatomic) NSDictionary *customEventData;
@property (nonatomic, nullable) NSString *networkEvent;
@property (nonatomic) BOOL networkEventsEnabled;

- (id) init;

- (id) initWithPush: (INPush *)push event: (EventType)event clickedButton:(int)clickedButton;
- (id) requestEventPush;
//
- (id) requestEventVisit;
//
-(id) initWithPermission: (EventTypePermission )event permission:(INPermissionType )permission;
-(id) initWithStringPermission: (NSString *)eventTypePermission permission:(NSString *)permissionType;
- (id) requestEventPermission;

-(id) initWithLocation: (EventTypeLocation)event lat:(double)latitude long:(double)longitud;
- (id) requestEventLocation;

- (id) initWithSendCustomEvent: (NSString *) customEvent customData:(NSDictionary *)customData;
- (id) requestEventCustom;

//var event is useless but is need to Xamarin
- (id) initWithNetworkEvent: (NSString *) networkEvent event:(Boolean)event;
- (id) requestNetworkEvent;
@end

NS_ASSUME_NONNULL_END
