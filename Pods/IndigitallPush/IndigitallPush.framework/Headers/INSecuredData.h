//
//  SecuredData.h
//  Indigitall
//
//  Created by indigitall on 05/06/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INSecuredData : NSObject

@property (nonatomic, class, readonly) NSString * deviceSecuredKey;
@property (nonatomic, class, readonly) NSString * deviceSecuredData;

+ (NSDictionary *) getDeviceSecuredKeys;
+ (NSString *)decryptPushWithSecuredKey: (NSString *)securedKey securedData:(NSString *)securedData;

@end

NS_ASSUME_NONNULL_END
