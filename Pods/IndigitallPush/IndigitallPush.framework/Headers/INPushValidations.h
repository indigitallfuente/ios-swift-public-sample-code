//
//  PushValidations.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INValidations.h>
NS_ASSUME_NONNULL_BEGIN

@interface INPushValidations : INValidations

@end

NS_ASSUME_NONNULL_END
