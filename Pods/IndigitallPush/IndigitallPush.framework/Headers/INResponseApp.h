//
//  ResponseApp.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INBaseResponse.h>
#import <IndigitallCore/INCoreExternalApp.h>
#import "INApplicationCallback.h"
NS_ASSUME_NONNULL_BEGIN

@interface INResponseApp : INBaseResponse
@property (nonatomic) NSArray<INCoreExternalApp *> *externalApps;
@property (nonatomic) NSDictionary *remoteConfiguration;

- (id) initWithCallback: (INApplicationCallback *_Nullable) callback;
- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error /*handler:(void (^)(BaseHandler *handler)) completion*/;

@end

NS_ASSUME_NONNULL_END
