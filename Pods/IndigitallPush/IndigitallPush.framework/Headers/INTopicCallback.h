//
//  TopicCallback.h
//  indigitall-objc
//
//  Created by indigitall on 06/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INTopic.h"
#import <IndigitallCore/INBaseCallback.h>

NS_ASSUME_NONNULL_BEGIN

@interface INTopicCallback : INBaseCallback
@property (nonatomic) NSMutableArray<INTopic*> *topic;

//typedef void(^topicCallback)(INTopic * _Nullable success, INError * _Nullable error);
//@property (readwrite, copy) topicCallback topicCallback;

typedef void(^onErrorTopic)(INError * error);
@property (readwrite, copy) onErrorTopic onError;

typedef void(^onSuccessTopic)(NSArray<INTopic *>*topics);
@property (readwrite, copy) onSuccessTopic onSuccess;

- (id)initOnSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;
@end

NS_ASSUME_NONNULL_END
