//
//  INPushDefaults.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INDefaults.h>
#import <IndigitallCore/INKeyChainService.h>
NS_ASSUME_NONNULL_BEGIN

@interface INPushDefaults : INDefaults

@property (nonatomic)NSString *pushTokenKey;
@property (nonatomic)NSString *pushPermissionAskedKey;
@property (nonatomic)NSString *pushPermissionStatusSentKey;
@property (nonatomic)NSString *pushPermissionModeKey;
@property (nonatomic)NSString *forceSimulatorToken;
@property (nonatomic)NSString *networkUpdateMinutesKey;
@property (nonatomic)NSString *networkEventsEnabledKey;
@property (nonatomic)NSString *networkSSIDKey;
@property (nonatomic)NSString *wifiFilterEnabledKey;
@property (nonatomic)NSString *deviceJsonKey;
@property (nonatomic)NSString *secureKeyKey;
@property (nonatomic)NSString *secureDataKey;
@property (nonatomic)NSString *putRequestKey;


+ (void) setPushToken:(NSString *) pushToken;
+ (NSString *) getPushToken;

+ (BOOL) getPushPermissionAsked;
+ (void) setPushPermissionAsked:(BOOL) pushPermissionAsked;

+ (int) getPushPermissionStatus;
+ (void) setPushPermissionStatus:(int) pushPermissionStatus;

+ (int) getPushPermissionMode;
+ (void) setPushPermissionMode:(int ) pushPermissionMode;

+ (void) setNetworkUpdateMinutes:(int)networkEventsEnabled;
+ (int) getNetworkUpdateMinutes;

+ (void) setWifiFilterEnabled:(BOOL)wifiFilterEnabled ;
+ (BOOL) getWifiFilterEnabled;

+ (void) setNetworkEventsEnabled:(BOOL)networkEventsEnabled;
+ (BOOL) getNetworkEventsEnabled;

+ (void) setNetworkSSID:(NSString *)networkSSID;
+ (NSString *) getNetworkSSID;

+ (void) setForceSimulatorToken :(BOOL)forceSimulatorToken ;
+ (BOOL) getForceSimulatorToken;

+ (void) setDeviceJson:(NSString *)deviceJson;
+ (NSString *) getDeviceJson;

+ (void) setSecureKey: (NSString *)setSecureKey;
+ (NSString *) getSecureKey;

+ (void) setSecureData: (NSString *)secureData;
+ (NSString *) getSecureData;

+ (int64_t) getPutRequestTimestamp;
+ (void) setPutRequestTimestamp:(int64_t) putRequest;
@end

NS_ASSUME_NONNULL_END
