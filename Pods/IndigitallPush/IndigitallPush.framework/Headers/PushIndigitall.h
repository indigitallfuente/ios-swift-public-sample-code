//
//  PushIndigitall.h
//  PushIndigitall
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>
#import "INPushConfig.h"
#import "INLocalNotifications.h"
#import "INResponseTopics.h"
#import "INSecuredData.h"
#import "INUIImageExtension.h"
#import "INCryptoExportImportManager.h"
#import "INRSA.h"
#import "INResponseDevice.h"
#import "NSData+Encryption.h"
#import "INResponseEvent.h"
#import "INDeviceCallback.h"
#import "INPush.h"
#import "INTopicCallback.h"
#import "INPermissions.h"
#import "INPushError.h"
#import "INAPIClient.h"
#import "INNotificationHandler.h"
#import "INResponseApp.h"
#import "INPermissionHandler.h"
#import "INPushLocationHandler.h"
#import "INApplicationCallback.h"
#import "INCurrentData.h"
#import "INNotificationServiceExtension.h"
#import "INWifiFilter.h"
#import "INAppHandler.h"
#import "INDeviceUtils.h"
#import "INPushValidations.h"
#import "INPushEventCallback.h"
#import "INPushLog.h"


NS_ASSUME_NONNULL_BEGIN

@interface PushIndigitall : NSObject

@property (nonatomic) NSString *domain;
@property (nonatomic) NSString *InAppdomain;

@property (nonatomic)PushIndigitall *current;

@property (nonatomic)NSMutableArray<NSNumber *> *notificationIdList;

@property (nonatomic) NSString *appKey;

//@property (nonatomic, class, readonly)NSString *inbox_AUTH_TOKEN_EXPIRE;

//@property (class) PermissionMode permissionModelocationPermission;

+ (void) initializeWithAppKey: (NSString *)appKey;

+ (void) initializeWithAppKey: (NSString *)appKey
      onIndigitallInitialized:(nullable void(^)( NSArray<INPermissions*> *permissions, INDevice *device))onIndigitallInitialized
      onErrorInitialized:(nullable void(^)(INError *onError))onErrorInitialized;

+ (void) initializeWithConfig: (INPushConfig *)config
      onIndigitallInitialized:(nullable void(^)(NSArray<INPermissions*> *permissions, INDevice *device))onIndigitallInitialized
      onErrorInitialized:(nullable void(^)(INError *onError))onErrorInitialized;

+ (void) getDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) enableDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) disableDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;

+ (void) topicsListWithOnSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void) topicsSubscribeWithTopic: (NSArray<INTopic *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsSubscribeWithArrayString: (NSArray<NSString *>*) topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsUnSubscribeWithArrayString: (NSArray<NSString *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsUnSubscribeWithTopic: (NSArray<INTopic *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void) askNotificationPermission;
+ (void) askLocationPermission;

+ (void) setDeviceToken: (NSData *)deviceToken;
+ (void) setDeviceStringToken: (NSString *)pushToken onNewUserRegistered: (nullable void(^)(INDevice *device))onNewUserRegistered;
+ (void) setDeviceToken: (NSData *)deviceToken onNewUserRegistered: (nullable void(^)(INDevice *device))onNewUserRegistered;


+ (void) handleWithNotification:(NSDictionary *)data identifier:(NSString *_Nullable)identifier DEPRECATED_ATTRIBUTE;
+ (void) handleWithNotification:(NSDictionary *)data identifier:(NSString *_Nullable)identifier withCompletionHandler:(nullable void(^)(INPush *push, INPushAction *action))completion DEPRECATED_ATTRIBUTE;
+ (void) handleWithData:(NSDictionary *)data identifier:(NSString *)identifier withCompletionHandler:(nullable void(^)(INPush *push, INPushAction *action))completion;
+ (void) handleWithResponse:(UNNotificationResponse *)response API_AVAILABLE(ios(10.0));
+ (void) handleWithResponse:(UNNotificationResponse *)response withCompletionHandler:(nullable void(^)(INPush *push,INPushAction *action))completion API_AVAILABLE(ios(10.0));

+ (void) performFetchWithCompletionHandler: (void(^)(UIBackgroundFetchResult handler))handler;

+ (void) receivedPushWithCompletion: (void(^)(INPush * push, int identifier))completion;


+ (void) registerDeviceForPush: (void(^)(INError *error,INDevice *device))completion;

+ (void) serviceDisabledVar: (void(^)(void))completion;
+ (void) serviceDisabledWithCompletion:(void (^)(void ))completion;

//+ (UNNotificationContent *) didReceive: (UNNotificationRequest *)request notificationContent:(UNMutableNotificationContent *)notificationContent API_AVAILABLE(ios(10.0));
//+ (UNNotificationContent *) serviceExtensionTimeWillExpire: (UNNotificationRequest *)request notificationContent:(UNMutableNotificationContent *)notificationContent API_AVAILABLE(ios(10.0));


+ (void) sendCustomEvent: (NSString *)eventCustom;
+ (void) sendCustomEvent: (NSString *)eventCustom customData:(nullable NSDictionary *)customData onSuccess:(nullable void(^)(void))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler API_AVAILABLE(ios(10.0));

+ (void)serviceExtensionTimeWillExpire: (UNMutableNotificationContent *)bestAttemptContent  withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler API_AVAILABLE(ios(10.0));

+ (UNNotificationPresentationOptions) willPresentNotification API_AVAILABLE(ios(10.0));

+ (void) setExternalCode: (NSString *) code onSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) logInWithId:(NSString *) code onSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) logOutWithSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;

+ (void) registerStatistics:(UNNotificationResponse *)response API_AVAILABLE(ios(10.0));
+ (void) getLocation:(void(^)(NSString *latitude, NSString *longitude))location;
@end

NS_ASSUME_NONNULL_END
