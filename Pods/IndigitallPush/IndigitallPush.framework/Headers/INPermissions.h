//
//  INPermissions.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UserNotifications/UserNotifications.h>
#import <CoreLocation/CoreLocation.h>
#import "UIKit/UIKit.h"

#import "INPermissionType.h"
#import "INPermissionStatus.h"

NS_ASSUME_NONNULL_BEGIN

@interface INPermissions : NSObject
@property (nonatomic) INPermissionStatus permissionStatus;
@property (nonatomic) INPermissionType permissionType;

//@property (nonatomic) INPermissionStatus locationPermission;
//@property (nonatomic) INPermissionStatus pushPermission;


- (id)init;
//
//- (id)initWithPermissions: (INPermissionType)type status:(INPermissionStatus) status;

- (NSArray<INPermissions*> *)getPermissions;
- (INPermissionStatus)requestLocationPermission;
- (INPermissionStatus)requestPushPermission;
- (NSArray *)onPushPermissionChange;
- (NSArray *)onLocationPermissionChange;
//- (NSString *)permissionToString: (NSString *)permissionType permission:(int) permission;
//- (int)permissionToInt: (NSString *)permission;


@end

NS_ASSUME_NONNULL_END
