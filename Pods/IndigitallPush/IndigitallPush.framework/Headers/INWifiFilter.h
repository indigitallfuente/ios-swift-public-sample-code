//
//  INWifiFilter.h
//  Indigitall
//
//  Created by indigitall on 30/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INWifiFilter : NSObject
+ (NSString *)fetchSSIDInfo;

@end

NS_ASSUME_NONNULL_END
