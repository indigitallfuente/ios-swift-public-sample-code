//
//  INTopic.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INAPIConstants.h>
#import <IndigitallCore/INCoreTopic.h>
NS_ASSUME_NONNULL_BEGIN

@interface INTopic : INCoreTopic



@end

NS_ASSUME_NONNULL_END
