//
//  PushEventCallback.h
//  IndigitallPush
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INEventCallback.h>
NS_ASSUME_NONNULL_BEGIN

@interface INPushEventCallback : INEventCallback

@end

NS_ASSUME_NONNULL_END
