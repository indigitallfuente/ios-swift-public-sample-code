//
//  DeviceUtils.h
//  Indigitall
//
//  Created by indigitall on 21/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INDevice.h"
#import "INPushDefaults.h"
#import "INTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INDeviceUtils : NSObject
+ (INDevice *) stringtoDevice: (NSString *)device;
+ (NSString *) toString: (INDevice *)device;
+ (INDevice *) compareDevices: (INDevice *)newDevice;
+ (BOOL) compareExternalApps: (NSArray<INCoreExternalApp*>*)externalApplications toCompare:(NSArray<INCoreExternalApp*>*)externalApplicationsTwo;
+ (NSArray *)externalAppsToJsonWithArray:(NSArray<INCoreExternalApp *>*)externalApps;
+ (NSArray<NSNumber *>*)getAppsIdFromExternalApps: (NSArray<INCoreExternalApp *> *) externalApplications;
+ (NSArray<NSString *>*) topicsToArray: (NSArray<INTopic *> *)topics;
@end

NS_ASSUME_NONNULL_END
