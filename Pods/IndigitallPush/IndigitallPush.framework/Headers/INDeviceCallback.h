//
//  DeviceCallback.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INBaseCallback.h>
#import "INDevice.h"
NS_ASSUME_NONNULL_BEGIN

@interface INDeviceCallback : INBaseCallback
@property (nonatomic) INDevice *device;

typedef void(^onErrorDevice)(INError * error);
@property (readwrite, copy) onErrorDevice onError;

typedef void(^onSuccessDevice)(INDevice * device);
@property (readwrite, copy) onSuccessDevice onSuccess;

- (id)initWithOnSuccess: (void(^)(INDevice * device))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
