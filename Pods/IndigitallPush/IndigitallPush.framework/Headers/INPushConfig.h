//
//  PushConfig.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INCoreConfig.h>
#import "INPushAuthMode.h"
NS_ASSUME_NONNULL_BEGIN

@interface INPushConfig : INCoreConfig

- (BOOL) configEnabled;
// MARK: - Properties

@property (nonatomic) INPushConfig *pushCurrent;
/// The push permission mode - automatic by default
@property (nonatomic) PermissionMode pushPermissionMode;
/// The minimun background fetch interval - 3600 miliseconds by default
@property (nonatomic) NSTimeInterval minimumBackgroundFetchInterval;
/// Host of the indigitall backend/enviroment
@property (nonatomic) NSString *domainInbox;
//@property (nonatomic) NSString * productVersion;
//@property (nonatomic) NSString* productName;
@property (nonatomic) BOOL wifiFilterEnabled;
@property (nonatomic) BOOL networkEventsEnabled;
@property (nonatomic) int networkUpdateMinutes;
@property (nonatomic) BOOL inAppEnabled;
@property (nonatomic) INAuthMode pushAuthMode;
/// Enable if is used to compile a simulator to get a fake token to work property
@property (nonatomic) BOOL forceSimulatorToken;

@property (nonatomic) BOOL secureSendingEnabled;
@property (nonatomic) NSString *secureSendingAppPublicKey;

+ (id) pushCurrent;
- (id) init;
- (id) initWithAppKey:(NSString *) appKey;
- (id) initWithJSON: (NSDictionary *)json;
- (void) enabled:(BOOL)enabled;

//- (void) updateWithConfig:(INConfig *)config;
- (void) updateConfig;


@end

NS_ASSUME_NONNULL_END
