//
//  UIImageExtension.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface INUIImageExtension : UIImageView
- (void)downloadedFromURL: (NSURL *)url contentMode: (int )mode;
- (void)downloadedFromLink: (NSString *)link contentMode: (int )mode;
@end

NS_ASSUME_NONNULL_END
