//
//  PushLocationHandler.h
//  Indigitall
//
//  Created by indigitall on 21/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <IndigitallCore/INCoreLocationHandler.h>

NS_ASSUME_NONNULL_BEGIN

@interface INPushLocationHandler : INCoreLocationHandler

@property (nonatomic)INPushLocationHandler * _Nullable pushLocationShared;

- (id) init;
- (void) configLocationHasChanged;
- (void) askPermission;
- (void) askPermissionBySystem;

- (void) startMonitoring;
- (void) sendAskEvent;
- (void) sendEvent:(int )status;

- (void) locationManager:(CLLocationManager *_Nullable)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
- (void) locationManager:(CLLocationManager *_Nullable)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations;
- (void) fetchSSIDInfo;



@end

NS_ASSUME_NONNULL_END
