    //
//  CustomerIndigitall.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INCustomer.h"
#import "INCustomerField.h"
#import <IndigitallCore/INError.h>
#import <IndigitallCore/INChannel.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomerIndigitall :NSObject

+ (void) getCustomerWithSuccess: (void(^)(INCustomer *customer))success failed:(void(^)(INError *error))failed;
+ (void) getCustomerInformationWithFieldNames: (NSArray<NSString *> * _Nullable)fieldNames success:(void(^)(NSArray<INCustomerField *> *customerFields))success failed:(void(^)(INError *error))failed;
+ (void) updateValueToCustomerFieldsWithFields: (NSDictionary *)fields success:(void(^)(INCustomer *customer))success failed:(void(^)(INError *error))failed;
+ (void) deleteValuesFromCustomerFieldsWithFieldNames: (NSArray<NSString *> *)fieldNames success:(void(^)(INCustomer *customer))success failed:(void(^)(INError *error))failed;
+ (void) linkCustomerWithChannel: (INChannel)channel success:(void(^)(void))success failed:(void(^)(INError *error))failed;
+ (void) unlinkCustomerWithChannel:(INChannel)channel  success:(void(^)(void))success failed:(void(^)(INError *error))failed;
@end

NS_ASSUME_NONNULL_END
