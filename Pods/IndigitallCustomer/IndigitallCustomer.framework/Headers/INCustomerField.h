//
//  CustomerField.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerField : NSObject

@property (nonatomic) NSString *customerFieldKey;
@property (nonatomic) NSString *customerFieldValue;

- (id) initWithKey: (NSString *)key value:(NSString *)value;
- (id) initWithJson: (NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
