//
//  Customer.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCustomer : NSObject

@property (nonatomic, class, readonly) NSString *ID;
@property (nonatomic, class, readonly) NSString *CUSTOMER_ID;
@property (nonatomic, class, readonly) NSString *APPLICATION_ID;
@property (nonatomic, class, readonly) NSString *CREATED_AT;
@property (nonatomic, class, readonly) NSString *UPDATED_AT;

@property (nonatomic) NSString *customerId;
@property (nonatomic) NSString *applicationId;
@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *createdAt;
@property (nonatomic) NSString *updatedAt;

- (id) initWithJson: (NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
