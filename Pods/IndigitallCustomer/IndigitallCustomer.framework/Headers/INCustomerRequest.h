//
//  CustomerRequest.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <IndigitallCore/INBaseRequest.h>
#import <Foundation/Foundation.h>
#import <IndigitallCore/INChannel.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerRequest : INBaseRequest

@property (nonatomic) NSString *customerId;
@property (nonatomic) NSDictionary *fields;
@property (nonatomic) NSArray<NSString *> *fieldNames;
@property (nonatomic) NSString *link;
@property (nonatomic) INChannel channel;
@property (nonatomic) NSString *deviceId;

- (id) init;
- (id) getCustomerRequest;
- (id) getCustomerFieldRequest;
- (id) putCustomerFieldRequest;
- (id) deleteCutomerFieldRequest;
- (id) postAndDeleteCustomerLinkRequest;

@end

NS_ASSUME_NONNULL_END
