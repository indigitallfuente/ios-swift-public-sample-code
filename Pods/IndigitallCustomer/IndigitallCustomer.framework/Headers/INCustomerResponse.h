//
//  CustomerResponse.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INBaseResponse.h>

NS_ASSUME_NONNULL_BEGIN

@interface INCustomerResponse : INBaseResponse

- (id)initWithCallback:(INBaseCallback *_Nullable)callback;

- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error;


@end

NS_ASSUME_NONNULL_END
