//
//  IndigitallCustomer.h
//  IndigitallCustomer
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>

#import "INCustomerClient.h"
#import "INCustomerResponse.h"
#import "INCustomerRequest.h"
#import "INCustomerFieldCallback.h"
#import "INCustomerLinkCallback.h"
#import "CustomerIndigitall.h"
#import "INCustomerField.h"
#import "INCustomerErrorCode.h"
#import "INCustomerError.h"
#import "INCustomer.h"
#import "INCustomerDefaults.h"
#import "INCustomerValidations.h"

//! Project version number for IndigitallCustomer.
FOUNDATION_EXPORT double IndigitallCustomerVersionNumber;

//! Project version string for IndigitallCustomer.
FOUNDATION_EXPORT const unsigned char IndigitallCustomerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IndigitallCustomer/PublicHeader.h>


