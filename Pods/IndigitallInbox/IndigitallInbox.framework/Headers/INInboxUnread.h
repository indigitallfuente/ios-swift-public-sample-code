//
//  @interface InboxUnread : NSObject    InboxUnread.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInboxUnread : NSObject

@property (nonatomic) int count;
@property (nonatomic) NSString *lastAccess;

- (id) init;

- (id) initWithJson: (NSDictionary *)json;


@end

NS_ASSUME_NONNULL_END
