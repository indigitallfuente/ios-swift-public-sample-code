//
//  InboxCategory.h
//  Indigitall
//
//  Created by indigitall on 21/10/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInboxCategory : NSObject

@property (nonatomic) int idCategory;
@property (nonatomic) NSString *code;
@property (nonatomic) NSString *name;

- (id) initWithDictionary: (NSDictionary *) dic;

@end

NS_ASSUME_NONNULL_END
