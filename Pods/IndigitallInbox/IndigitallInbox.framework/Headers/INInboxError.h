//
//  INInboxError.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INError.h>
#import "INInboxErrorCode.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxError :INError

@property (readwrite, nonatomic) INInboxErrorCode inboxErrorCode;

- (id)initWithErrorCode:(INInboxErrorCode)errorCode descriptionMessage:(nullable NSString *)descriptionMessage;

@end

NS_ASSUME_NONNULL_END
