//
//  INInboxPush.h
//  IndigitallInbox
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INCorePush.h>
#import "INInboxAction.h"
#import "INInboxPushButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxPush : INCorePush
/// An action that will execute when the user interacts with the push
@property (nonatomic) INInboxAction *action;
/// An array of push available buttons
@property (nonatomic) NSArray<INInboxPushButton *> *buttons;

- (id)init: (NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
