//
//  IndigitallInbox.h
//  IndigitallInbox
//
//  Created by indigitall on 8/3/23.
//

#import <Foundation/Foundation.h>

#import "INInboxClient.h"
#import "INInboxConstants.h"
#import "INInboxAuthenticationRequest.h"
#import "INInboxCountersRequest.h"
#import "INInboxPushRequest.h"
#import "INInboxAuthenticationResponse.h"
#import "INInboxResponse.h"
#import "INInboxAuthenticationCallback.h"
#import "INInboxBaseCallback.h"
#import "INInboxCallback.h"
#import "INInboxCountersCallback.h"
#import "INInboxGetAuthConfig.h"
#import "INInboxNotificationsCallback.h"
#import "INInbox.h"
#import "INInboxCategory.h"
#import "INInboxStatus.h"
#import "INInboxAuthMode.h"
#import "INInboxCounters.h"
#import "INInboxError.h"
#import "INInboxErrorCode.h"
#import "INInboxNotification.h"
#import "INInboxStatus.h"
#import "INInboxUnread.h"
#import "INInboxDefaults.h"

//! Project version number for IndigitallInbox.
FOUNDATION_EXPORT double IndigitallInboxVersionNumber;

//! Project version string for IndigitallInbox.
FOUNDATION_EXPORT const unsigned char IndigitallInboxVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <IndigitallInbox/PublicHeader.h>


