//
//  INInboxCounters.h
//  Indigitall
//
//  Created by indigitall on 19/03/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInboxUnread.h"



@interface INInboxCounters : NSObject

@property (nonatomic) int click;
@property (nonatomic) int sent;
@property (nonatomic) int deleted;
@property (nonatomic) INInboxUnread* unread;

- (id) init;
- (id) initWithJson: (NSDictionary *)json;

@end


