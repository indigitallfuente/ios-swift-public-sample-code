//
//  INInboxErrorCode.h
//  Indigitall
//
//  Created by indigitall on 23/5/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum {
    INBOX_GET_ERROR = 3001,
    INBOX_EXTERNAL_ID_NO_REGISTERED = 3003,
    INBOX_IS_REQUESTING_PAGE = 3006,
    INBOX_GET_PAGE_ERROR = 3011,
    INBOX_GENERAL_ERROR = 3600,
    INBOX_BAD_REQUEST = 3604
} INInboxErrorCode;

extern NSString* _Nonnull const FormatStateInboxError_toString[];
NS_ASSUME_NONNULL_END
