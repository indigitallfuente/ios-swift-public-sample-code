//
//  InAppDao.h
//  Indigitall
//
//  Created by indigitall on 10/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInApp.h"
#import "INDBContentProvider.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppDao : NSObject

+ (NSArray *)setContentValue: (INInApp *)inApp;

+ (BOOL) addNewInApp: (INInApp *) inApp;
+ (BOOL) deleteInApp: (int) inAppId;
+ (BOOL) clearInAppDatabase;
+ (BOOL) updateInAppField: (INInApp *) inApp key:(NSString *)key value:(NSObject *)value ;
+ (INInApp *) searchInApp: (NSString *) inAppCode;
@end

NS_ASSUME_NONNULL_END
