//
//  InAppAPIClient.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppLog.h"
#import <IndigitallCore/INBaseClient.h>
#import "INRequestInApp.h"
#import "INEventRequestInApp.h"
#import "INInAppCustomerRequest.h"
#import "INInAppCallback.h"
#import "INFormRequestInApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppAPIClient : INBaseClient
+ (void) getInAppCampaign:(INRequestInApp *)request completion:(INInAppCallback *)callback;
+ (void) loadInApp: (INRequestInApp *)request completion:(INInAppCallback *)callback;

//+ (void) postEventRequest: (EventRequestInApp *)request completion:(BaseCallback *)callback;
+ (void) getTopics: (INBaseRequest *)request completion:(INBaseCallback *)callback;
+ (void) postEventClickRequest: (INEventRequestInApp *)request completion:(INBaseCallback *)callback;
+ (void) postEventPrintRequest: (INEventRequestInApp *)request completion:(INBaseCallback *)callback;

+ (void) postEventForm: (INFormRequestInApp *)request completion:(INBaseCallback *)callback;

+ (void) putCustomerField: (INInAppCustomerRequest *)request;
@end

NS_ASSUME_NONNULL_END
