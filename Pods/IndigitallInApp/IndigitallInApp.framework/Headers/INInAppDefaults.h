//
//  INInAppDefaults.h
//  Indigitall
//
//  Created by indigitall on 20/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INDefaults.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppDefaults : INDefaults


@property (nonatomic)NSString *domainInApp;
@property (nonatomic)NSString *inAppShowOnceKey;
@property (nonatomic)NSString *inAppTimesClickedKey;
@property (nonatomic)NSString *dismissForeverKey;
@property (nonatomic)NSString *getInAppWasGotKey;
@property (nonatomic)NSString *topicListSubscribedKey;

+ (void) setDomainInApp:(NSString *)domainInApp;
+ (NSString *) getDomainInApp;

+ (void) setInAppShowOnce:(NSString *)showOnce;
+ (NSString *) getInAppShowOnce;

+ (void) setInAppTimesClicked:(NSString *)timesClicked;
+ (NSString *) getInAppTimesClicked;

+ (void) setInAppDismissForever:(NSString *)dismissForever;
+ (NSString *) getInAppDismissForever;

+ (void) setInAppWasGot:(NSString *)inAppWasGot;
+ (NSString *) getInAppWasGot;

+ (void) setInAppTopicListSubscribed:(NSArray *)inAppTopicListSubscribed;
+ (NSArray *) getInAppTopicListSubscribed;
@end

NS_ASSUME_NONNULL_END
