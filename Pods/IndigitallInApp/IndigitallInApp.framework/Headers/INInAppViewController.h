//
//  INInAppViewController.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INInAppError.h"
#import "INInAppHandler.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppViewController : UIViewController<UIGestureRecognizerDelegate, WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

@property (nonatomic) void (^didAppear)(void);
@property (nonatomic) void (^didTouch)(void);
@property (nonatomic) void (^didDismissed)(void);
@property (nonatomic) void (^didCancel)(void);
@property (nonatomic) void (^onShowTimeFinished)(INInApp *inApp, int showTime);
@property (strong, nonatomic) void (^failed)(NSArray<INError *> *error);
@property (strong, nonatomic) void (^formSubmit)(INInApp *inApp);
@property (nonatomic) WKUserContentController *contentController;
@property (nonatomic) WKWebView *webView;
@property (nonatomic) INInApp *inapp;
@property (strong, nonatomic) NSString *noAction;

- (id) initWithContent:(NSString *)content inApp: (INInApp *)inApp closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled failed:(void(^)(NSArray<INInAppError*> *error))failed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

@end

NS_ASSUME_NONNULL_END
