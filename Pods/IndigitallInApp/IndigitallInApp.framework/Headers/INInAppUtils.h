//
//  InAppUtils.h
//  Indigitall
//
//  Created by indigitall on 12/3/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INInApp.h"
#import "INInAppError.h"
#import "INInAppDefaults.h"
#import <IndigitallCore/INDateUtils.h>
#import "INEventRequestInApp.h"
#import "INInAppAPIClient.h"
#import "INInAppAPIConstants.h"
#import "INInAppTopic.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppUtils : NSObject

+ (void) inAppWasGot: (NSString *)inAppId
          didExpired:(void(^)(INInApp *inApp, INInAppError *error))didExpired
                didFound: (void(^)(INInApp *inApp))didfound
            notFound: (void(^)(void))notFound
   didDismissForever: (void(^)(INInApp *inApp, INInAppError *error))didDismissForever
    didShowManyTimes: (void(^)(INInApp *inApp, INInAppError *error))didShowManyTimes
         didClickOut: (void(^)(INInApp *inApp, INInAppError *error))didClickOut;

+ (void) addNewInApp:(INInApp *)inApp;
+ (void) inAppWasTappedWithInApp:(INInApp *)inApp;

+ (void) inAppOpenUrl:  (INInAppAction *) action;

+ (BOOL) isInAppDismissForever: (INInApp *)inApp DEPRECATED_ATTRIBUTE;
+ (void) addNewInAppToDismissForever:(INInApp *)inApp DEPRECATED_ATTRIBUTE;
+ (void) inAppWasShownWithInApp: (INInApp *)inApp
                     didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired
            didShowMoreThanOnce:(nullable void(^)(INInApp *inApp, INError *error))didShowMoreThanOnce
                    didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut
                      onSuccess: (void(^)(void))onSuccess DEPRECATED_ATTRIBUTE;
+ (NSString *) setExternalCode: (NSString *) code;
@end

NS_ASSUME_NONNULL_END
