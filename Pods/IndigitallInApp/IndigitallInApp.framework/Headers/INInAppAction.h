//
//  INInAppAction.h
//  Indigitall
//
//  Created by indigitall on 04/03/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INCoreAction.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppAction : INCoreAction

@property (nonatomic) int actionId;
- (id) init: (NSDictionary *)json;
+ (NSDictionary *)toJson: (INInAppAction *)action;
+ (NSString *) toString: (INInAppAction *)action;
@end

NS_ASSUME_NONNULL_END
