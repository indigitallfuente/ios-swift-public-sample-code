//
//  INInAppTopic.h
//  inapp
//
//  Created by indigitall on 29/6/22.
//

#import <Foundation/Foundation.h>
#import <IndigitallCore/INCoreTopic.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppTopic : INCoreTopic
@property (nonatomic) int topicId;
- (id) initWithJson: (NSDictionary *)json;
+ (NSDictionary *) toJson: (INInAppTopic *)topic;
@end

NS_ASSUME_NONNULL_END
