//
//  InAppIndigitall.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppView.h"
#import "INInAppViewController.h"
#import "INInAppCallback.h"
#import <UIKit/UIKit.h>
#import "INInAppConfig.h"
#import "INInAppTopic.h"
#import "INInAppTopicCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface InAppIndigitall : NSObject
@property (nonatomic, readonly)NSString *domainInApp;

//@property (nonatomic, readonly)Indigitall *indigitallView;
+ (void) showInAppWithInAppId:(NSString *)inAppId view: (UIView *)view success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed;

+ (void) showInAppWithAppKey:(NSString *)appKey inAppId:(NSString *)inAppId view: (UIView *)view success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed;

+ (void) showInAppWithConfig:(INInAppConfig *)config InAppId: (NSString *)inAppId view: (UIView *)view didTouchWithAction:(nullable void(^)(INInAppAction *action))action onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed;

+ (void) showInAppWithConfig:(INInAppConfig *)config InAppId: (NSString *)inAppId view: (UIView *)view didTouchWithAction:(nullable void(^)(INInAppAction *action))action onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray<INInAppError*> *error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (void) showMultipleInAppWithListInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed;

+ (void) showMultipleInAppWithAppKey:(NSString *)appKey listInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed;

+ (void) showMultipleInAppWithConfig:(INInAppConfig *)config listInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView didTouch:(nullable void(^)(INInApp *inApp,UIView *webView, INInAppAction *action))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed;

+ (void) showMultipleInAppWithConfig:(INInAppConfig *)config listInAppId:(NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView didTouch:(nullable void(^)(INInApp *inApp,UIView *webView, INInAppAction *action))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray<INInAppError*> *error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (void) showPopupWithPopupId:(NSString *)popupId didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel failed:(nullable void(^)(INError *error))failed;

+ (void) showPopupWithAppKey:(NSString *)appKey popupId:(NSString *)popupId didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel failed:(nullable void(^)(INError *error))failed;

+ (void) showPopupWithConfig:(INInAppConfig *)config popupId:(NSString *)popupId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didTouchWithAction:(nullable void(^)(INInAppAction *action))action didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever failed:(nullable void(^)(INError *error))failed;

+ (void) showPopupWithConfig:(INInAppConfig *)config popupId:(NSString *)popupId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didTouchWithAction:(nullable void(^)(INInAppAction *action))action didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray<INInAppError*> *error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

+ (void) topicsListWithOnSuccess: (nullable void(^)(NSArray<INInAppTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsSubscribeWithArrayString: (NSArray<NSString *>*) topics onSuccess: (nullable void(^)(NSArray<NSString *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsUnSubscribeWithArrayString: (NSArray<NSString *>*)topics onSuccess: (nullable void(^)(NSArray<NSString *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (NSString *) setExternalCode: (NSString *) code;
@end

NS_ASSUME_NONNULL_END
