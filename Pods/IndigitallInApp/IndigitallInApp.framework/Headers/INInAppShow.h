//
//  INInAppShow.h
//  Indigitall
//
//  Created by indigitall on 10/6/22.
//  Copyright © 2022 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppShow : NSObject

@property (nonatomic, class, readonly) NSString *TIMES_SHOWED;
@property (nonatomic, class, readonly) NSString *TIMES_CLICKED;
@property (nonatomic, class, readonly) NSString *WAS_DISMISSED;
@property (nonatomic) int timesShowed;
@property (nonatomic) int timesClicked;
@property (nonatomic) BOOL wasDismissed;

- (id)initWithNumberOfShowed: (int)numberOfShowed timesClicked: (int) timesClicked wasDismissed: (BOOL)wasDismissed;

- (id) initWithNSDictionary: (NSDictionary *)json;
- (id)initWithNSString: (NSString *) jsonString;

- (NSDictionary *)toJson;

- (NSString *) toString;


@end

NS_ASSUME_NONNULL_END
