//
//  INInAppForm.h
//  IndigitallInApp
//
//  Created by indigitall on 22/12/22.
//

#import <Foundation/Foundation.h>
#import "INInAppFormInput.h"
NS_ASSUME_NONNULL_BEGIN

@interface INInAppForm : NSObject
@property (nonatomic) NSString *formId;
@property (nonatomic) NSArray<INInAppFormInput *> *inAppInputs;

- (id) initWithDictionary: (NSDictionary *)json;
@end

NS_ASSUME_NONNULL_END
