//
//  InAppHandler.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

#import "INInApp.h"
#import "INInAppError.h"
#import "INInAppViewController.h"
#import <IndigitallCore/INUIApplicationExtension.h>

NS_ASSUME_NONNULL_BEGIN
@class INInAppView;
@interface INInAppHandler  : NSObject

@property (nonatomic) int width;
@property (nonatomic) int height;

@property (nonatomic,strong) NSURL *inAppURL;

- (id)init;

- (void)showInApp: (NSString *)inAppCode view: (UIView *)view didTouchWithAction:(nullable void(^)(INInAppAction *inAppAction))action onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray <INInAppError *>*error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

- (void) showMultipleInApp: (NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView didTouch:(nullable void(^)(INInApp *inApp,UIView *webView, INInAppAction *inAppAction))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray <INInAppError *>*error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

- (void) showPopup:(NSString *)popUpCode closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didTouchWithAction:(nullable void(^)(INInAppAction *inAppAction))action didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished didExpired:(nullable void(^)(INInApp *inApp, INError *error))didExpired didShowManyTimes:(nullable void(^)(INInApp *inApp, INError *error))didShowManyTimes didClickOut:(nullable void(^)(INInApp *inApp, INError *error))didClickOut didDismissForever:(nullable void(^)(INInApp *inApp, INError *error))didDismissForever failed:(nullable void(^)(INError *error))failed formFailed:(nullable void(^)(NSArray <INInAppError *>*error))formFailed formSubmit:(nullable void(^)(INInApp *inApp))formSubmit;

- (void) getInApp:(NSString *)inAppId success:(void(^)(NSString * content,INInApp *inApp))success errorLoad:(void(^)(INError *error))errorLoad DEPRECATED_ATTRIBUTE;
+ (void) getInApp:(NSString *)inAppId success:(void(^)(INInApp *inApp))success errorLoad:(void(^)(INError *error))errorLoad;

+ (void) loadContent:(INInApp *)inApp success:(void(^)(NSString * content,INInApp *inApp))success failed:(void(^)(INError *error))failed;


@end

NS_ASSUME_NONNULL_END

