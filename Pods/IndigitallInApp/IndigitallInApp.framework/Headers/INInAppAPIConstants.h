//
//  InAppAPIConstants.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppAPIConstants : NSObject

@property (nonatomic, class, readonly) NSString *domain;
@property (nonatomic, class, readonly) NSString *pathInApp;
@property (nonatomic, class, readonly) NSString *_id;
@property (nonatomic, class, readonly) NSString *inAppId;
@property (nonatomic, class, readonly) NSString *showOnce;
@property (nonatomic, class, readonly) NSString *properties;
@property (nonatomic, class, readonly) NSString *action;
@property (nonatomic, class, readonly) NSString *url;
@property (nonatomic, class, readonly) NSString *contentUrl;
@property (nonatomic, class, readonly) NSString *schema;
@property (nonatomic, class, readonly) NSString *code;
@property (nonatomic, class, readonly) NSString *width;
@property (nonatomic, class, readonly) NSString *height;
@property (nonatomic, class, readonly) NSString *showTime;
@property (nonatomic, class, readonly) NSString *typeNoAction;
@property (nonatomic, class, readonly) NSString *NUMBER_OF_SHOWS;
@property (nonatomic, class, readonly) NSString *NUMBER_OF_CLICKS;
@property (nonatomic, class, readonly) NSString *CREATION_DATE;
@property (nonatomic, class, readonly) NSString *EXPIRED_DATE;
@property (nonatomic, class, readonly) NSString *LAST_VERSION_ID;
@property (nonatomic, class, readonly) NSString *DISMISS_FOREVER;
@property (nonatomic, class, readonly) NSString *ACTION_ID;
@property (nonatomic, class, readonly) NSString *CACHE_Ttl;
@property (nonatomic, class, readonly) NSString *INAPP_SHOW;
@property (nonatomic, class, readonly) NSString *INAPP_VERSION;
@property (nonatomic, class, readonly) NSString *INAPP_CUSTOM_DATA;
@property (nonatomic, class, readonly) NSString *INAPP_SCRIPT;

@property (nonatomic, class, readonly) NSString *FORM_ID;
@property (nonatomic, class, readonly) NSString *FORM_NAME;
@property (nonatomic, class, readonly) NSString *FORM_TYPE;
@property (nonatomic, class, readonly) NSString *FORM_REQUIRED;
@property (nonatomic, class, readonly) NSString *FORM_SEND_TO_CUSTOMER;
@property (nonatomic, class, readonly) NSString *FORM_INPUTS;

@end

NS_ASSUME_NONNULL_END
